package no.velthoven.util;

import no.velthoven.util.ValueValidator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.junit.*;

import javax.json.JsonString;
import javax.json.JsonValue;
import java.util.Date;

@RunWith(JUnit4.class)
public class ValueValidatorTest {

    @Test
    public void ensureString() throws Throwable {
        ValueValidator.Result<String> res = null;

        /*res = ValueValidator.parseString("test", "");
        Assert.assertEquals(res.value(), "");
        Assert.assertTrue(res.valid());

        res = ValueValidator.parseString("test", null);
        Assert.assertFalse(res.valid());
        Assert.assertNull(res.value());

        res = ValueValidator.parseString("test", null, false);
        Assert.assertFalse(res.valid());
        Assert.assertNull(res.value());

        res = ValueValidator.parseString("test", null, true);
        Assert.assertTrue(res.valid());
        Assert.assertNull(res.value());

        res = ValueValidator.parseString("test", "a", false, 1, 1);
        Assert.assertTrue(res.valid());
        Assert.assertEquals(res.value(), "a");

        res = ValueValidator.parseString("test", "ab", false, 1, 1);
        Assert.assertFalse(res.valid());
        Assert.assertNull(res.value());

        res = ValueValidator.parseString("test", "b", false, 2, 2);
        Assert.assertFalse(res.valid());
        Assert.assertNull(res.value());*/
    }
    @Test
    public void ensureLong() throws Throwable {
        ValueValidator.Result<Long> res = null;

        /*res = ValueValidator.parseLong("test", "0");
        Assert.assertEquals(res.value(), new Long(0));
        Assert.assertTrue(res.valid());

        res = ValueValidator.parseLong("test", null);
        Assert.assertFalse(res.valid());
        Assert.assertNull(res.value());

        res = ValueValidator.parseLong("test", null, false);
        Assert.assertFalse(res.valid());
        Assert.assertNull(res.value());

        res = ValueValidator.parseLong("test", null, true);
        Assert.assertTrue(res.valid());
        Assert.assertNull(res.value());

        res = ValueValidator.parseLong("test", "0", false, 0, 1);
        Assert.assertTrue(res.valid());
        Assert.assertEquals(res.value(), new Long(0));

        res = ValueValidator.parseLong("test", "2", false, 1, 1);
        Assert.assertFalse(res.valid());
        Assert.assertNull(res.value());

        res = ValueValidator.parseLong("test", "1", false, 2, 2);
        Assert.assertFalse(res.valid());
        Assert.assertNull(res.value());*/
    }
    @Test
    public void ensureDouble() throws Throwable {
        ValueValidator.Result<Double> res = null;

       /* res = ValueValidator.parseDouble("test", "0.1");
        Assert.assertEquals(res.value(), new Double(0.1));
        Assert.assertTrue(res.valid());

        res = ValueValidator.parseDouble("test", null);
        Assert.assertFalse(res.valid());
        Assert.assertNull(res.value());

        res = ValueValidator.parseDouble("test", null, false);
        Assert.assertFalse(res.valid());
        Assert.assertNull(res.value());

        res = ValueValidator.parseDouble("test", null, true);
        Assert.assertTrue(res.valid());
        Assert.assertNull(res.value());

        res = ValueValidator.parseDouble("test", "0.1", false, 0, 1);
        Assert.assertTrue(res.valid());
        Assert.assertEquals(res.value(), new Double(0.1));

        res = ValueValidator.parseDouble("test", "2.1", false, 1, 1);
        Assert.assertFalse(res.valid());
        Assert.assertNull(res.value());

        res = ValueValidator.parseDouble("test", "1.1", false, 2, 2);
        Assert.assertFalse(res.valid());
        Assert.assertNull(res.value());*/
    }

    @Test
    public void ensureDate() throws Throwable {
        ValueValidator.Result<Date> res = null;

        /*res = ValueValidator.parseDate("test", "1997-07-16T19:20:30.45+01:00");
        //Assert.assertEquals(res.value().toString(), new Date("1997-07-16T19:20:30.45+01:00"));
        Assert.assertTrue(res.valid());

        res = ValueValidator.parseDate("test", null);
        Assert.assertFalse(res.valid());
        Assert.assertNull(res.value());

        res = ValueValidator.parseDate("test", null, false);
        Assert.assertFalse(res.valid());
        Assert.assertNull(res.value());

        res = ValueValidator.parseDate("test", null, true);
        Assert.assertTrue(res.valid());
        Assert.assertNull(res.value());

        res = ValueValidator.parseDate("test", "3000-07-16T19:20:30.45+01:00", false, new Date(Long.MIN_VALUE), new Date(Long.MAX_VALUE));
        Assert.assertTrue(res.valid());
        //Assert.assertEquals(res.value(), new Double(0.1));

        res = ValueValidator.parseDate("test", "3000-07-16T19:20:30.45+01:00", false, new Date(Long.MIN_VALUE), new Date());
        Assert.assertFalse(res.valid());
        Assert.assertNull(res.value());

        res = ValueValidator.parseDate("test", "2010-07-16T19:20:30.45+01:00", false, new Date(), new Date(Long.MAX_VALUE));
        Assert.assertFalse(res.valid());
        Assert.assertNull(res.value());*/

    }

}