package no.velthoven.proto;

import net.sf.json.JSONObject;
import no.velthoven.util.ValueValidator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONWriter;

import javax.json.*;
import javax.servlet.http.HttpServletResponse;
import java.io.Reader;
import java.io.Writer;
import java.util.Calendar;
import java.util.Map;

import static java.util.Calendar.*;

/**
 * Created by thv on 26.04.14.
 */
public class TransitionRecordMarshaller {

    private final static Log log = LogFactory.getLog(TransitionRecordMarshaller.class);

    static public void writeJson(TransitionRecord tr, Writer w){

        JSONWriter jw = new JSONWriter(w);
        jw.object().
            key("label").value(tr.getItem_label()).
            key("from").value(tr.getItem_state_from()).
            key("to").value(tr.getItem_state_to()).
            key("longitude").value(tr.getLongitude()).
            key("latitude").value(tr.getLatitude()).
            key("time").value(javax.xml.bind.DatatypeConverter.printDateTime(tr.getFired())).
            key("user").value(tr.getUser_id()).
            endObject();


    }

    static public TransitionRecord parseJson(Reader r) throws Throwable{

        TransitionRecord transitionRecord = new TransitionRecord();
        JsonReader reader = Json.createReader(r);
        JsonStructure jsonst = reader.read();
        if(jsonst.getValueType() != JsonValue.ValueType.OBJECT)
            throw new Exception("root json node must be an object");

        JsonObject jsonTransitionObject = (JsonObject)jsonst;

        for (Map.Entry<String, JsonValue> entry : jsonTransitionObject.entrySet())
        {
            switch (entry.getKey())
            {
                case "label":
                    transitionRecord.setItem_label(
                            ValueValidator.ensureString(entry.getKey(), entry.getValue()));
                    break;
                case "from":
                    transitionRecord.setItem_state_from(
                            ValueValidator.ensureString(entry.getKey(), entry.getValue()));
                    break;
                case "to":
                    transitionRecord.setItem_state_to(
                            ValueValidator.ensureString(entry.getKey(), entry.getValue()));
                    break;
                case "longitude":
                    transitionRecord.setLongitude(
                            ValueValidator.ensureDouble(entry.getKey(), entry.getValue()));
                    break;
                case "latitude":
                    transitionRecord.setLatitude(
                            ValueValidator.ensureDouble(entry.getKey(), entry.getValue()));
                    break;
                case "time":
                    transitionRecord.setFired(
                            ValueValidator.ensureCalendar(entry.getKey(), entry.getValue()));
                    break;
                case "user":
                    transitionRecord.setUser_id(
                            ValueValidator.ensureString(entry.getKey(), entry.getValue()));
                    break;

            }
            System.out.println(entry.getKey() + "/" + entry.getValue());
        }

        return transitionRecord;

    }
    static public void writeXml(TransitionRecord tr, Writer w){

    }
    static public TransitionRecord parseXml(Reader r){
        return null;
    }


}
