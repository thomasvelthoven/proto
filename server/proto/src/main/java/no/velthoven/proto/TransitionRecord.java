package no.velthoven.proto;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by thv on 23.04.14.
 */
public class TransitionRecord {

    private int id;
    private String user_id;
    private Calendar fired;
    private Calendar posted;
    private Double longitude;
    private Double latitude;
    private String item_label;
    private String item_type;
    private String item_state_from;
    private String item_state_to;
    private String item_group;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public Calendar getFired() {
        return fired;
    }

    public void setFired(Calendar fired) {
        this.fired = fired;
    }

    public Calendar getPosted() { return posted; }

    public void setPosted(Calendar posted) { this.posted = posted;}

    public String getItem_label() {
        return item_label;
    }

    public void setItem_label(String item_label) {
        this.item_label = item_label;
    }

    public String getItem_type() {
        return item_type;
    }

    public void setItem_type(String item_type) {
        this.item_type = item_type;
    }

    public String getItem_state_from() {
        return item_state_from;
    }

    public void setItem_state_from(String item_state_from) {
        this.item_state_from = item_state_from;
    }

    public String getItem_state_to() {
        return item_state_to;
    }

    public void setItem_state_to(String item_state_to) {
        this.item_state_to = item_state_to;
    }

    public String getItem_group() {
        return item_group;
    }

    public void setItem_group(String item_group) {
        this.item_group = item_group;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }
}
