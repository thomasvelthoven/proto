package no.velthoven.proto;

import org.apache.abdera.protocol.server.Provider;
import org.apache.abdera.protocol.server.impl.DefaultProvider;
import org.apache.abdera.protocol.server.impl.SimpleWorkspaceInfo;
import org.apache.abdera.protocol.server.servlet.AbderaServlet;

/**
 * Created by thv on 23.04.14.
 */
public final class TransitionRecordProviderServlet extends AbderaServlet {

    protected Provider createProvider() {
        TransitionRecordCollectionAdaptor ca = new TransitionRecordCollectionAdaptor();
        ca.setHref("transition_record");

        SimpleWorkspaceInfo wi = new SimpleWorkspaceInfo();
        wi.setTitle("Transition Record Workspace");
        wi.addCollection(ca);

        DefaultProvider provider = new DefaultProvider("/");
        provider.addWorkspace(wi);

        provider.init(getAbdera(), null);
        return provider;
    }
}

