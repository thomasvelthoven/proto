package no.velthoven.proto;

import org.apache.abdera.Abdera;
import org.apache.abdera.factory.Factory;
import org.apache.abdera.i18n.iri.IRI;
import org.apache.abdera.model.Content;
import org.apache.abdera.model.Person;
import org.apache.abdera.protocol.server.RequestContext;
import org.apache.abdera.protocol.server.context.ResponseContextException;
import org.apache.abdera.protocol.server.impl.AbstractEntityCollectionAdapter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.http.HttpServletResponse;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;


public class TransitionRecordCollectionAdaptor extends AbstractEntityCollectionAdapter<TransitionRecord> {

    private final static Log log = LogFactory.getLog(TransitionRecordCollectionAdaptor.class);

    private static final String ID_PREFIX = "tag:acme.com,2007:TransitionRecord:entry:";

    private AtomicInteger nextId = new AtomicInteger(1000);
    private Map<Integer, TransitionRecord> transition_records = new HashMap<Integer, TransitionRecord>();
    private Factory factory = new Abdera().getFactory();

    public String[] getAccepts(RequestContext request) {
        return new String[] {"application/atom+json;type=entry" };
    }

    /**
     * A unique ID for this feed.
     */
    public String getId(RequestContext request) {
        return "tag:acme.com,2007:transition_record:feed";
    }

    /**
     * The title of our collection.
     */
    public String getTitle(RequestContext request) {
        return "Thovel Checklist Transition Records";
    }

    /**
     * The author of this collection.
     */
    public String getAuthor(RequestContext request) {
        return "Thovel Checklists";
    }

    // END SNIPPET: feedmetadata
    // START SNIPPET: getEntries
    public Iterable<TransitionRecord> getEntries(RequestContext request) {
        return transition_records.values();
    }

    // END SNIPPET: getEntries
    // START SNIPPET: getEntry
    public TransitionRecord getEntry(String resourceName, RequestContext request) throws ResponseContextException {
        Integer id = getIdFromResourceName(resourceName);
        return transition_records.get(id);
    }

    private Integer getIdFromResourceName(String resourceName) throws ResponseContextException {

        System.out.print("getIdFromResourceName(String "+resourceName+")");
        int idx = resourceName.indexOf("-");
        if (idx == -1) {
            throw new ResponseContextException(404);
        }
        return new Integer(resourceName.substring(0, idx));
    }

    public String getName(TransitionRecord entry) {
        return entry.getId() + "-" + entry.getItem_label().replaceAll(" ", "_") + "_" + entry.getItem_state_to().replaceAll(" ", "_");
    }

    // END SNIPPET: getEntry
    // START SNIPPET: entryMetadata
    public String getId(TransitionRecord entry) {
        return ID_PREFIX + entry.getId();
    }

    public String getTitle(TransitionRecord entry) {
        return entry.getItem_label();
    }

    public Date getUpdated(TransitionRecord entry) {
        return entry.getFired().getTime();
    }

    public List<Person> getAuthors(TransitionRecord entry, RequestContext request) throws ResponseContextException {
        Person author = request.getAbdera().getFactory().newAuthor();
        author.setName("Acme Industries");
        return Arrays.asList(author);
    }

    public Object getContent(TransitionRecord entry, RequestContext request) {
        Content content = factory.newContent(Content.Type.TEXT);
        StringWriter sw = new StringWriter();
        TransitionRecordMarshaller.writeJson(entry,sw);
        content.setText(sw.toString());
        return content;
    }

    // END SNIPPET: entryMetadata
    // START SNIPPET: methods
    public TransitionRecord postEntry(String title,
                              IRI id,
                              String summary,
                              Date updated,
                              List<Person> authors,
                              Content content,
                              RequestContext request) throws ResponseContextException {


        /*if(!content.getContentType().equals("application/json"))
            throw new ResponseContextException("content type must be application/json", HttpServletResponse.SC_PRECONDITION_FAILED);*/

        if(null == content.getValue())
            throw new ResponseContextException("content value cannot be null", HttpServletResponse.SC_PRECONDITION_FAILED);

        log.error(content.getContentType());

        StringReader srJsonContent = new StringReader(content.getValue());

        try {
            TransitionRecord tr = TransitionRecordMarshaller.parseJson(srJsonContent);
            tr.setId(nextId.getAndIncrement());
            transition_records.put(tr.getId(), tr);
            return tr;
        } catch (Throwable throwable) {
            throw new ResponseContextException(HttpServletResponse.SC_PRECONDITION_FAILED,throwable);
        }

    }

    public void putEntry(TransitionRecord TransitionRecord,
                         String title,
                         Date updated,
                         List<Person> authors,
                         String summary,
                         Content content,
                         RequestContext request) throws ResponseContextException {
        //TransitionRecord.setName(content.getText().trim());
    }

    public void deleteEntry(String resourceName, RequestContext request) throws ResponseContextException {
        Integer id = getIdFromResourceName(resourceName);
        transition_records.remove(id);
    }
    // END SNIPPET: methods
}



