package no.velthoven.util;

public class ValueValidatorException extends Throwable
{
    public ValueValidatorException(String message)
    {
        super(message);
    }
    public ValueValidatorException(String message, Throwable throwable)
    {
        super(message, throwable);
    }
}
