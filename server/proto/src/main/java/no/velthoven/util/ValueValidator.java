package no.velthoven.util;

import javax.json.JsonNumber;
import javax.json.JsonString;
import javax.json.JsonValue;
import java.util.Calendar;
import java.util.Calendar;
import java.util.Date;


/**
 * Created by thv on 24.04.14.
 */
public class ValueValidator {


    public static class Result<T>
    {
        private T value;
        private String error;
        private boolean valid;

        public Result(T aValue, String anError)
        {
            this.error = anError;
            this.valid = anError == null;
            this.value = aValue;
        }
        public Result(T aValue)
        {
            this(aValue, null);
        }

        public String error() {
            return error;
        }

        public boolean valid() {
            return valid;
        }

        public T value() {
            return value;
        }

    }

    static public Result<String> parseString(
            String name,
            JsonValue jsonValue
    ) throws Throwable {
        return parseString(name, jsonValue, false, 0, 100);
    }

    static public String ensureString(
            String name,
            JsonValue jsonValue
    ) throws Throwable {
        return ensureString(name, jsonValue, false, 0, 100);
    }

    static public Result<String> parseString(
            String name,
            JsonValue jsonValue,
            boolean nullAllowed
    ) throws Throwable {
        return parseString(name, jsonValue, nullAllowed, 0, 100);
    }

    static public String ensureString(
            String name,
            JsonValue jsonValue,
            boolean nullAllowed
    ) throws Throwable {
        return ensureString(name, jsonValue, nullAllowed, 0, 100);
    }

    static public Result<String> parseString(
            String name,
            JsonValue jsonValue,
            boolean nullAllowed,
            int minLength,
            int maxLength
    ) throws Throwable {

        if (name == null)
            throw new Exception("parameter name cannot be null");

        if(!nullAllowed){
            if(jsonValue == null)
                return new Result<String>(null, "value for " + name +" cannot be null");
        }
        else if(nullAllowed && jsonValue == null)
            return new Result<String>(null);

        // jsonValue is not null

        if(jsonValue.getValueType() != JsonValue.ValueType.STRING)
            return new Result<String>(null, "JsonValue for "+ name +"should be a string");

        JsonString jsonString = (JsonString)jsonValue;
        String theString = jsonString.getString();

        int length = theString.length();

        int minLengthLocal = minLength;
        int maxLengthLocal = maxLength;

        if(length < minLengthLocal)
            return new Result<String>(null, name +" must be at least " + minLengthLocal + " but is " + length+ " characters long");

        if(length > maxLengthLocal)
            return new Result<String>(null, name +" must can be max " + maxLengthLocal + " but is " + length + " characters long");

        return new Result<String>(theString);

    }
    static public String ensureString(
            String name,
            JsonValue jsonValue,
            boolean nullAllowed,
            int minLength,
            int maxLength
    ) throws Throwable {
        Result<String> res;
        res = parseString(name, jsonValue, nullAllowed, minLength, maxLength);
        if(!res.valid())
            throw new ValueValidatorException(res.error());
        return res.value();
    }

    static public Long ensureLong(
            String name,
            JsonValue jsonValue
    ) throws Throwable {
        return ensureLong(
                name,
                jsonValue,
                false,
                Long.MIN_VALUE,
                Long.MAX_VALUE);
    }
    static public Result<Long> parseLong(
            String name,
            JsonValue jsonValue
            ) throws Throwable {
        return parseLong(
                name,
                jsonValue,
                false,
                Long.MIN_VALUE,
                Long.MAX_VALUE);
    }
    static public Long ensureLong(
            String name,
            JsonValue jsonValue,
            boolean nullAllowed
    ) throws Throwable {
        return ensureLong(
                name,
                jsonValue,
                nullAllowed,
                Long.MIN_VALUE,
                Long.MAX_VALUE);
    }
    static public Result<Long> parseLong(
            String name,
            JsonValue jsonValue,
            boolean nullAllowed
            ) throws Throwable {
        return parseLong(
                name,
                jsonValue,
                nullAllowed,
                Long.MIN_VALUE,
                Long.MAX_VALUE);
    }
    static public Result<Long> parseLong(
            String name,
            JsonValue jsonValue,
            boolean nullAllowed,
            long minValue,
            long maxValue
    )throws Throwable {

        if (name == null)
            throw new Exception("parameter name cannot be null");

        if(!nullAllowed){
            if(jsonValue == null)
                return new Result<Long>(null, "value for " + name +" cannot be null");
        }
        else if(nullAllowed && jsonValue == null)
            return new Result<Long>(null);

        // jsonValue is not null
        if(jsonValue.getValueType() != JsonValue.ValueType.NUMBER)
            return new Result<Long>(null, name +" must be a number");

        JsonNumber jsonNumber = (JsonNumber)jsonValue;
        Double theDouble = jsonNumber.doubleValue();
        if(theDouble != theDouble.longValue())
            return new Result<Long>(null, name +"should not have a fraction");

        Long theLong = theDouble.longValue();

        if(theLong < minValue)
            return new Result<Long>(null, name +" must be at least " + minValue + " but is " + theLong);

        if(theLong > maxValue)
            return new Result<Long>(null, name +" must can be max " + maxValue + " but is " + theLong);

        return new Result<Long>(theLong);

    }

    static public Long ensureLong(
            String name,
            JsonValue jsonValue,
            boolean nullAllowed,
            long minValue,
            long maxValue
    ) throws Throwable {
        Result<Long> res = parseLong(
                name,
                jsonValue,
                nullAllowed,
                minValue,
                maxValue);
        if (!res.valid())
            throw new ValueValidatorException(res.error());
        return res.value();
    }


    static public Double ensureDouble(
            String name,
            JsonValue jsonValue
    ) throws Throwable {
        return ensureDouble(
                name,
                jsonValue,
                false,
                0,
                Double.MAX_VALUE);
    }
    static public Result<Double> parseDouble(
            String name,
            JsonValue jsonValue
    ) throws Throwable {
        return parseDouble(
                name,
                jsonValue,
                false,
                0,
                Double.MAX_VALUE);
    }
    static public Double ensureDouble(
            String name,
            JsonValue jsonValue,
            boolean nullAllowed
    ) throws Throwable {
        return ensureDouble(
                name,
                jsonValue,
                nullAllowed,
                0,
                Double.MAX_VALUE);
    }
    static public Result<Double> parseDouble(
            String name,
            JsonValue jsonValue,
            boolean nullAllowed
    ) throws Throwable {
        return parseDouble(
                name,
                jsonValue,
                nullAllowed,
                0,
                Double.MAX_VALUE);
    }
    static public Result<Double> parseDouble(
            String name,
            JsonValue jsonValue,
            boolean nullAllowed,
            double minValue,
            double maxValue
    )throws Throwable {

        if (name == null)
            throw new Exception("parameter name cannot be null");

        if(!nullAllowed){
            if(jsonValue == null)
                return new Result<Double>(null, "value for " + name +" cannot be null");
        }
        else if(nullAllowed && jsonValue == null)
            return new Result<Double>(null);

        // jsonValue is not null

        if(jsonValue.getValueType() != JsonValue.ValueType.NUMBER)
            return new Result<Double>(null, name +" must be a number");

        JsonNumber jsonNumber = (JsonNumber)jsonValue;
        Double theDouble = jsonNumber.doubleValue();

        if(theDouble < minValue)
            return new Result<Double>(null, name +" must be at least " + minValue + " but is " + theDouble);

        if(theDouble > maxValue)
            return new Result<Double>(null, name +" must can be max " + maxValue + " but is " + + theDouble);


        return new Result<Double>(theDouble);

    }

    static public Double ensureDouble(
            String name,
            JsonValue jsonValue,
            boolean nullAllowed,
            double minValue,
            double maxValue
    )throws Throwable {
        Result<Double> res;
        res = parseDouble(name, jsonValue, nullAllowed, minValue, maxValue);
        if(!res.valid())
            throw new ValueValidatorException(res.error());
        return res.value();
    }


    static public Calendar ensureCalendar(
            String name,
            JsonValue jsonValue
    ) throws Throwable {
        Calendar cal = Calendar.getInstance();
        return ensureCalendar(
                name,
                jsonValue,
                false,
                new Date(Long.MIN_VALUE),
                new Date(Long.MAX_VALUE)
        );
    }
    static public Result<Calendar> parseCalendar(
            String name,
            JsonValue jsonValue
    ) throws Throwable {
        return parseCalendar(
                name,
                jsonValue,
                false,
                new Date(Long.MIN_VALUE),
                new Date(Long.MAX_VALUE)
        );
    }

    static public Calendar ensureCalendar(
            String name,
            JsonValue jsonValue,
            boolean nullAllowed
    ) throws Throwable {
        return ensureCalendar(
                name,
                jsonValue,
                nullAllowed,
                new Date(Long.MIN_VALUE),
                new Date(Long.MAX_VALUE)
        );
    }
    static public Result<Calendar> parseCalendar(
            String name,
            JsonValue jsonValue,
            boolean nullAllowed
    ) throws Throwable {
        return parseCalendar(
                name,
                jsonValue,
                nullAllowed,
                new Date(Long.MIN_VALUE),
                new Date(Long.MAX_VALUE)
        );
    }
    static public Result<Calendar> parseCalendar(
            String name,
            JsonValue jsonValue,
            boolean nullAllowed,
            Date maxHistory,
            Date maxFuture
    )throws Throwable {

        if (name == null)
            throw new Exception("parameter name cannot be null");

        if(!nullAllowed){
            if(jsonValue == null)
                return new Result<Calendar>(null, "value for " + name +" cannot be null");
        }
        else if(nullAllowed && jsonValue == null)
            return new Result<Calendar>(null);

        if(jsonValue.getValueType() != JsonValue.ValueType.STRING)
            return new Result<Calendar>(null, name +" must be a string formatted as an ISO Calendar");

        JsonString jsonString = (JsonString) jsonValue;
        String theString = jsonString.getString();


        int length = theString.length();

        int minLengthLocal = 1;
        int maxLengthLocal = 50;

        if(length < minLengthLocal)
            return new Result<Calendar>(null, name +" must be at least " + minLengthLocal + " but is " + length+ " characters long");

        if(length > maxLengthLocal)
            return new Result<Calendar>(null, name +" must can be max " + maxLengthLocal + " but is " + length + " characters long");

        Calendar theCalendar = javax.xml.bind.DatatypeConverter.parseDateTime(theString);

        if(theCalendar.before(maxHistory))
            return new Result<Calendar>(null, name + "is too old");

        if(theCalendar.after(maxFuture))
            return new Result<Calendar>(null, name + "is too young");


        return new Result<Calendar>(theCalendar);

    }
    static public Calendar ensureCalendar(
            String name,
            JsonValue jsonValue,
            boolean nullAllowed,
            Date maxHistory,
            Date maxFuture
    )throws Throwable {
        Result<Calendar> res;
        res = parseCalendar(name, jsonValue, nullAllowed, maxHistory, maxFuture);
        if(!res.valid())
            throw new ValueValidatorException(res.error());
        return res.value();
    }


}
