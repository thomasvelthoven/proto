curl -v \
--no-keepalive \
 -X POST \
-d @$1 \
-H "Content-Type:application/atom+xml" \
http://localhost:8080/proto/transition_record \
| xmllint --format -
