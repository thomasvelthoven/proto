#!/bin/bash
#cp record1.json record1.$1.json
#replace 
#  "self": "http://10.0.0.241/proto/www/data/records/record1.json", 
#with 
#  "self": "http://10.0.0.241/proto/www/data/records/record1.$1.json",
#  "next": "http://10.0.0.241/proto/www/data/records/record1.$2.json",

# first van current naar first


#bash archive.sh record1 15 16

function badargexit {
    echo "usage: archive.sh <file> <current archive id> <next archive id>"
    exit 1
}


filearg=$1
if [ "$filearg" == "" ]; then
    echo "no file found"
    badargexit
fi

if [ -f $filearg ]; then
    echo "operating on $filearg"
else
    echo "arg1 is not a file"
    badargexit
fi

currentarchiveid=$2
if [ "$currentarchiveid" == "" ]; then
    echo "no currentarchiveid found"
    badargexit
fi
if [ "$(echo "$currentarchiveid" | sed -n 's/^.*\(\.\).*$/\1/p')" != "" ]; then
    echo "currentarchiveid should not have a dot"
    badargexit
fi

nextarchiveid=$3
if [ "$nextarchiveid" == "" ]; then
    echo "no nextarchiveid found"
    badargexit
fi
if [ "$(echo "$nextarchiveid" | sed -n 's/^.*\(\.\).*$/\1/p')" != "" ]; then
    echo "nextarchiveid should not have a dot"
    badargexit
fi





if [ "$(echo "${filearg}" | sed -n 's/^.*\(\/\).*$/\1/p')" == "" ]; then
filename=$filearg
else
filename="$(echo "${filearg}" | sed -n 's/.*\/\([^\/]*\)$/\1/p')"
fi

#echo "filename $filename"

filenamenoex="$(echo "${filename}" | sed -n 's/\([^\.]*\).*$/\1/p')"

#echo "filenamenoex $filenamenoex"

#source base.sh
base="http://10.0.0.241/proto/www/data/records"

#"previous": "http://10.0.0.241/proto/www/data/records/record1.14.json",

firstfirst="${base}/${filenamenoex}.json"
efirstfirst=$(echo $firstfirst | sed -e 's/[]\/$*.^|[]/\\&/g')
first="${base}/${filenamenoex}.0.json"
efirst=$(echo $first | sed -e 's/[]\/$*.^|[]/\\&/g')
current="${base}/${filenamenoex}.json"
ecurrent=$(echo $current | sed -e 's/[]\/$*.^|[]/\\&/g')
self="${base}/${filenamenoex}.${currentarchiveid}.json"
eself=$(echo $self | sed -e 's/[]\/$*.^|[]/\\&/g')
next="${base}/${filenamenoex}.${nextarchiveid}.json"
enext=$(echo $next | sed -e 's/[]\/$*.^|[]/\\&/g')

preself='"self": "'
epreself=$(echo $preself | sed -e 's/[]\/$*.^|[]/\\&/g')
postself='",'
epostself=$(echo $postself | sed -e 's/[]\/$*.^|[]/\\&/g')

prenext='"next": "'
eprenext=$(echo $preself | sed -e 's/[]\/$*.^|[]/\\&/g')
postnext='",'
epostnext=$(echo $postself | sed -e 's/[]\/$*.^|[]/\\&/g')

sedstring="s/${epreself}${ecurrent}${epostself}/${preself}${eself}${postself}\n\t${prenext}${enext}${postnext}/"

#echo "$sedstring"

previous="${base}/${filenamenoex}"
eprevious=$(echo $previous | sed -e 's/[]\/$*.^|[]/\\&/g')
eprevious="${eprevious}\.[^\.]*\.json"

preprevious='"previous": "'
epreprevious=$(echo $preprevious | sed -e 's/[]\/$*.^|[]/\\&/g')
postprevious='",'
epostprevious=$(echo $postprevious | sed -e 's/[]\/$*.^|[]/\\&/g')

sedstringcurrent="s/${epreprevious}${eprevious}${epostprevious}/\t${preprevious}${eself}${postprevious}/"

prefirst='"first": "'
eprefirst=$(echo $prefirst | sed -e 's/[]\/$*.^|[]/\\&/g')
postfirst='",'
epostfirst=$(echo $postfirst | sed -e 's/[]\/$*.^|[]/\\&/g')

sedstringfirstfirst="s/${eprefirst}${efirstfirst}${epostfirst}/${prefirst}${efirst}${postfirst}/"
sedstringfirstfirstandprevious="s/${eprefirst}${efirstfirst}${epostfirst}/${preprevious}${eself}${postprevious}\n\t${prefirst}${efirst}${postfirst}/"


cat $filearg | sed -e "$sedstring" -e "$sedstringfirstfirst" > ${filenamenoex}.${currentarchiveid}.json
mv $filename $filename.bak
cat $filename.bak | sed  -e '/\"events\"/q' > $filename.bak2
echo -e "\t]" >> $filename.bak2
echo "}" >> $filename.bak2
cat $filename.bak2 | sed -e "$sedstringcurrent" -e "$sedstringfirstfirstandprevious" > $filename
