#!/bin/bash

function badargexit {
    echo "usage"
    exit 1
}

curl -X POST -d @postevents.json http://localhost:4000/record1.json --header "Content-Type:application/json"
echo
curl -H "If-None-Match: ${etagrecord1}" -X GET http://localhost:4000/record1.json
echo
etagrecord1=$(curl -s -I -H "If-None-Match: ${etag}" -X GET http://localhost:4000/record1.json | sed -n -e 's/^ETag: \(.*\)$/\1/p')
export etagrecord1

curl -X POST -d @postevents.json http://localhost:4000/record2.json --header "Content-Type:application/json"
echo
curl -H "If-None-Match: ${etagrecord2}" -X GET http://localhost:4000/record2.json
echo
etagrecord2=$(curl -s -I -H "If-None-Match: ${etag}" -X GET http://localhost:4000/record2.json | sed -n -e 's/^ETag: \(.*\)$/\1/p')
export etagrecord2
