'use strict';


protoControllers.controller('ProtoBodyCtrl', ['$scope', '$sce', '$http', '$routeParams', '$interval', 'subjectManager','originInfo',//'snapRemote',
    function ($scope, $sce, $http, $routeParams, $interval, subjectManager, originInfo) { //, snapRemote

        /*$scope.sessionColors = { }
        $scope.sessionColors[originInfo.session] = "";*/

        var refresher;

        var subject = subjectManager.subscribeTo("http://proto.thovel.net/data/records/record2.json", function(rfSubjectId, rfEvent){
            $scope.launchRfEvent("rfItemlist", rfSubjectId, rfEvent, false);
        });

        $scope.start = function () {
            if (angular.isDefined(refresher))
                return;

            var msSinceLAstMaintenance = 0;
            refresher = $interval(function () {
                subjectManager.refreshAll($http)


                msSinceLAstMaintenance+=300;
                if(msSinceLAstMaintenance > 100000){
                    originInfo.doSlotMaintenance(subjectManager);
                    originInfo.sendAllHeartBeats(subjectManager, $scope)
                    msSinceLAstMaintenance = 0;
                }

            }, 300);


            refresher.catch(function () {
                $scope.stop();
            })
        }

        $scope.stop = function () {
            if (angular.isDefined(refresher)) {
                $interval.cancel(refresher);
                refresher = undefined;
            }
        };

        $scope.$on('$destroy', function () {
            // Make sure that the interval is destroyed too
            $scope.stop();
        });


        $scope.sessionId = uuid.v1();

        if ($scope.inventory == undefined) {
            var urlInventory = 'data/repo/index.json';
            var futureResponseInventory = $http.get(urlInventory);

            futureResponseInventory.success(
                function (data, status, headers, config) {
                    $scope.inventory = data;
                }
            );

            futureResponseInventory.error(
                function (data, status, headers, config) {
                    throw new Error('Something went wrong getting ' + url + ' ...');
                }
            );
        }

        $scope.$on('itemRequestEditing', function (event, data) {

            if (data.length != 1)
                throw "expected one argument being an item got " + arguments.length;

            $scope.item2bedited = data[0];
            $scope.modalTitle = "Edit item:" + $scope.item2bedited.label;

            $('#myModal').modal('toggle')

        });

        $scope.$on('itemRequestRemove', function (event, data) {

            if (data.length != 1)
                throw "expected one argument being an item got " + data.length;

            var item2bremoved = data[0];


        });


        $scope.$on('itemRequestAdding', function (event, data) {

            $scope.item2bedited = createNewItem(createItemId(), "", "", "");
            $scope.item2bedited.type = 'check';
            $scope.item2bedited.group = data[1];
            $scope.modalTitle = "New item";
            $('#myModal').modal('toggle')

        });

        $scope.launchRfEvent = function (rfSubjectType, rfSubjectId, rfEvent, localCall) {
            if (rfSubjectType != "rfItemlist")
                throw Error("currently only support rfItemList events")
            if (rfSubjectId == undefined)
                throw Error("rfSubjectId must contain an id")
            if (rfEvent == undefined)
                throw Error("rfEvent must be an rfEvent")

            if(localCall){
                $scope.$broadcast('RfEvent', [rfSubjectType, rfSubjectId, rfEvent])
                subjectManager.logEvent(rfSubjectId, rfEvent);
                subjectManager.publishRecentLocalEvents(subjectManager.subjects[rfSubjectId], $http,2);
            }
            else if(rfEvent.origin.session != originInfo.session){
                $scope.$broadcast('RfEvent', [rfSubjectType, rfSubjectId, rfEvent])
            }



        }

        $scope.$on('RfEvent', function (event, data) {

            var rfSubjectType = data[0];
            var rfSubjectId = data[1];
            var rfEvent = data[2];

            if (rfSubjectType != "rfItemlist")
                throw Error("currently only support rfItemList events")

            if ($scope.itemlists == undefined)
                $scope.itemlists = {};

            var subject = subjectManager.subjects[rfSubjectId];
            if(subject == undefined){
                subject = subjectManager.createSubject(rfSubjectId, undefined);
                subjectManager.subjects[rfSubjectId] = subject;
            }
            if(subject.data== undefined)
                subject.data = createNewGroup(rfSubjectId, "", "");


            var slotId
            var user
            if(rfEvent.origin.session == originInfo.session){
                slotId = 0;
                user = "";
            }
            else{
                slotId = originInfo.slotId(new Date(rfEvent.time), rfEvent.origin);
                user = rfEvent.origin.user
            }

            applyItemlistEvent(subject.data, rfEvent, slotId, user);

            if($scope.checklist == undefined)
                $scope.checklist = subject.data;


        });


    }

]);
