'use strict';

function SubjectManager(originInfo){

    this.subjects = {};
    this.subs = {};
    this.data = undefined;
    var subjectManager = this;

    this.subscribeTo = function(currentUrl, onEvent){
        return this.subjectByUrl(currentUrl, onEvent);
    }
    this.subjectByUrl = function(subjectUrl, onEvent){
        var retSubject;
        var subjectInSubs = this.subs[subjectUrl];
        if(subjectInSubs == undefined){
            for(var iSubject=0;iSubject < this.subjects.length ; iSubject++){
                if(this.subjects[iSubject].currentUrl == subjectUrl){
                    retSubject = this.subjects[iSubject]
                }
            }
            if(retSubject==undefined){
                retSubject = this.createSubject(undefined, subjectUrl);
            }
        }
        else{
            retSubject = subjectInSubs;
        }
        retSubject.onEvent = onEvent;
        return retSubject;
    }

    this.createSubject = function(id, url){
        var retSubject =
        {
            subjectId: id,
            title: undefined,
            lastEventId: undefined,
            lastLocalEventId: undefined,
            group: undefined,
            ETag: undefined,
            currentUrl: url,
            nextPage: undefined,
            previousPage: undefined,
            lastUpdate: 0,
            secondsSinceLastUpdate: 0,
            localEvents: [],
            data: undefined

        }
        if(id != undefined)
            this.subjects[id] = retSubject;

        if(url != undefined)
            this.subs[url] = retSubject;

        return retSubject;
    }

    this.subscribeToPreviousPage = function(sub, previousPage){
        if(sub == undefined)
            throw Error("sub must have a value here")
        if(previousPage == undefined)
            throw Error("previousPage must have a value here")
        sub.nextPage = undefined;
        sub.previousPage = previousPage;
    }
    this.subscribeToNextPage = function(sub, nextPage){
        if(sub == undefined)
            throw Error("sub must have a value here")
        if(nextPage == undefined)
            throw Error("nextPage must have a value here")
        sub.previousPage = undefined;
        sub.nextPage = nextPage;
    }
    this.subscribeToCurrentUrl = function(sub){
        if(sub == undefined)
            throw Error("sub must have a value here")
        sub.previousPage = undefined;
        sub.nextPage = undefined;
    }
    this.refreshAll = function($http){

        for(var url in subjectManager.subs){
            var sub = subjectManager.subs[url];
            subjectManager.refreshSub($http, sub);
            subjectManager.publishRecentLocalEvents(sub, $http,1);
        }
    }
    this.refreshSub = function($http, sub) {

        var timeout = 1000;
        var futureSub = undefined;
        var ETag = sub.ETag;
        var toCurrent = false;
        var toPast = false;
        var checkCurrent = false;

        if (sub.nextPage != undefined) {
            // the last known event has been found and we are on our way to find all events since then
            toCurrent = true;
            futureSub = $http.get(sub.nextPage, {timeout: timeout});
        }
        else if (sub.previousPage != undefined) {
            // we are looking for the last known event in previous pages
            toPast = true;
            futureSub = $http.get(sub.previousPage, {timeout: timeout});
        }
        else {
            // we might be up-to-date but must check
            checkCurrent = true;
            futureSub = $http.get(sub.currentUrl, {timeout: timeout});
        }


        futureSub.success(
            function (data, status, headersf, config) {
                if(sub.subjectId == undefined){
                    sub.subjectId = data.id;
                    sub.title = data.title;
                    subjectManager.subjects[data.id] = sub;
                }
                var headers = headersf();

                var newEvents = undefined;
                var currentIsChanged = false;

                if(checkCurrent) {

                    if (sub.ETag != headers.etag) {
                        sub.ETag = headers.etag;
                        currentIsChanged = true;
                    }
                }

                if(toCurrent){
                    // we have found where to start consuming events and are on the way to current
                    // lets consume all events we find here.
                    newEvents =  data.events;
                    ensureCorrectRfEventsContents(newEvents);
                    if(data.rel.next == undefined){
                        // no more recent archives so lets poll the current one
                        subjectManager.subscribeToCurrentUrl(sub);
                    }
                    else{
                        // there are more recent archives to consume.
                        subjectManager.subscribeToNextPage(sub, data.rel.next);
                    }
                }
                else if(toPast || currentIsChanged){
                    newEvents = findAllAfterEvent(data.events, sub.lastEventId);
                    if(newEvents == undefined)
                    {
                        // lastEventId is not found here
                        // can we look in an archive?
                        if(data.rel.previous != undefined) {
                            // found a previous archive. Lets go there next.
                            subjectManager.subscribeToPreviousPage(sub, data.rel.previous);
                        }
                        else{
                            // lastEventId is not found here and there is no archive to look
                            // have we seen events before?
                            if (sub.lastEventId == undefined) {
                                // we have never seen an event and we are in the oldest archive.
                                // lets consume the events we find here
                                newEvents =  data.events;
                                try {
                                    ensureCorrectRfEventsContents(newEvents);
                                }
                                catch(ex){
                                    console.log(config);
                                    throw ex;
                                }
                                // are there any newer archives that hold more relevant events?
                                if(data.rel.next == undefined) {
                                    // no more recent archives so lets poll the current one
                                    subjectManager.subscribeToCurrentUrl(sub);
                                }
                                else{
                                    // there are more recent archives to consume.
                                    subjectManager.subscribeToNextPage(sub, data.rel.next);
                                }
                            }
                            else {
                                // the last sub.lastEventId is not found
                                // this can only mean that the eventLogger does not know or has lost its history.
                                // We should tell it what has happened.
                                // But first we must find out how much it knows.
                                sub.lastEventId = undefined;
                                sub.lastLocalEventId = undefined;
                            }
                        }
                    }
                    else
                    {
                        ensureCorrectRfEventsContents(data.events);
                        if(data.rel.next == undefined){
                            subjectManager.subscribeToCurrentUrl(sub);
                            //subjectManager.publishRecentLocalEvents(sub, $http,4);
                        }
                        else {
                            subjectManager.subscribeToNextPage(sub, data.rel.next);
                        }
                    }
                }
                else{
                    // console.log('do nothing: '+headers['etag']);
                }

                if(newEvents != undefined){
                    // update the sub.lastEventId
                    // verify newEvents if any
                    // broadcast the remote events locally
                    // determine the last local event found remotely
                    if(newEvents.length > 0){
                        for(var iEvent = newEvents.length-1 ; iEvent >= 0 ; iEvent--){
                            var newEvent = newEvents[iEvent];
                            if (newEvent.origin.session == originInfo.session) {
                                sub.lastLocalEventId = newEvent.id;
                            }

                            if(sub.lastEventId == undefined) {
                                // be sure that we are not seeing an event we have already seen.
                                var foundEventInLocalEvents = false;
                                for (var iLocalEvent = 0; iLocalEvent < sub.localEvents; iLocalEvent++) {
                                    if (newEvent.id != sub.localEvents[iLocalEvent]) {
                                        foundEventInLocalEvents = true;
                                    }
                                }
                                if (foundEventInLocalEvents) {
                                    // We have seen this event before
                                    if (data.rel.next == undefined && iLocalEvent >= sub.localEvents) {
                                        // we have reached the newest event that this source knows about
                                        sub.lastEventId = newEvent.id;
                                    }

                                }
                                else{
                                    sub.lastEventId = newEvent.id;
                                    sub.onEvent(sub.subjectId, newEvent);
                                }
                            }
                            else {
                                // We have not seen this event before
                                sub.lastEventId = newEvent.id;
                                sub.onEvent(sub.subjectId, newEvent);
                            }


                        }
                    }
                }

                var previousUpdate = sub.lastUpdate;
                sub.lastUpdate = new Date();
                if(previousUpdate == 0 || previousUpdate == undefined){
                    sub.secondsSinceLastUpdate = 0;
                }
                else {
                    sub.secondsSinceLastUpdate = Math.floor((sub.lastUpdate.getTime() - previousUpdate.getTime()) / 1000);
                }

            }
        );

        futureSub.error(
            function (data, status, headersrf, config) {

                var headers = headersrf();
                for(var header in headers){
                    console.log(header+": "+headers[header]);
                }

                /*if(sub.lastUpdate == undefined)
                    sub.secondsSinceLastUpdate = 0;
                else
                    sub.secondsSinceLastUpdate = Math.floor((sub.lastUpdate.getTime() - previousUpdate.getTime()) / 1000);*/
            }
        );

    }

    this.logEvent = function(subjectId , rfEvent) {
        if(rfEvent.origin.session != originInfo.session)
            return;

        var subject = subjectManager.subjects[subjectId];
        subject.localEvents.splice(0, 0, rfEvent);

    }

    this.publishRecentLocalEvents = function(sub, $http, callerId){
        var recentLocalEvents;
        if(sub.lastEventId == undefined) {
            // If sub.lastEventId has not been initialized
            // sub.lastLocalEventId might not be the latest known to the source.
            recentLocalEvents = undefined;
        }
        else{
            recentLocalEvents = findAllAfterEvent(sub.localEvents, sub.lastLocalEventId, true);
        }

        if(recentLocalEvents != undefined){
            var postBuffer = "[";
            var first = true;
            for(var i =0;i<recentLocalEvents.length;i++){
                //console.log(callerId + " " +recentLocalEvents[i].id + " will be posted");
                if(first){
                    first = false;
                }
                else{
                    postBuffer += ",\n"
                }
                postBuffer += angular.toJson(recentLocalEvents[i]);

            }
            postBuffer += "]";
            if(recentLocalEvents.length > 0){
                var futurePostEvent = $http.post(sub.currentUrl, postBuffer , {timeout: 1000});
                futurePostEvent.success(
                    function (data, status, headersf, config) {
                        //console.log(callerId + " posted "+ recentLocalEvents.length +" recentLocalEvents");
                    }
                )
                futurePostEvent.error(
                    function (data, status, headers, config) {
                        console.log(callerId + " failed to post "+ recentLocalEvents.length +" recentLocalEvents");
                    }
                )
            }

        }
    }



}



