'use strict';

// Demonstrate how to register services
// In this case it is a simple value service.
protoServices.
    value('version', '0.1').
    service('originInfo', [OriginInfo]).
    service('subjectManager', ['originInfo', SubjectManager]);
