

function ensureCorrectItemlistEvents(ilEvents) {
    if(ilEvents == undefined || !ilEvents.isArray)
        throw Error("expected an array of Itemlistevent");
    for(var i=0;i<ilEvents.length;i++){
        ensureCorrectItemlistEvent(ilEvents[i]);
    }
}
function ensureCorrectItemlistEvent(ilEvent) {
    if(ilEvent == undefined)
        throw Error("expected an Itemlistevent");
    if(ilEvent.name == undefined || ilEvent.name == "")
        throw Error("an Itemlistevent should have a name that is not empty");
    if(ilEvent.params == undefined || ilEvent.params.isArray)
        throw Error("an Itemlistevent should have a params array");

}
/*function broadcastRemoteEventsLocally(url, mostRecentRemoteEventIdFoundLocally) {
    ilEvent.origSessionId = function sessionId;
}*/
/*function broadcastLocalEventLocally(ilEvent) {
    ensureCorrectItemlistEvent(ilEvent);
    ilEvent.origSessionId = function sessionId;
}*/
/*function postLocalEventsRemotely(mostRecentLocelEventIdFoundRemotely) {

}
function recordLocalEventLocally() {

}*/
/*function broadcastItemlistEvent(ilEvent) {

    if(ilEvent == undefined)
        throw Error("broadcastItemlistEvent expects an argument: ilEvent");

    function $broadcast(ilEvent.name, [ilEvent]);

}*/
