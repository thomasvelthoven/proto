var protoControllers =
    angular.module('protoControllers', []);

var protoServices =
    angular.module('protoServices', []);

var protoApp =
    angular.module('proto', ['ngTouch', 'ngRoute', 'ngSanitize', 'protoControllers', 'protoServices']); //, 'snap'

protoApp.config(['$routeProvider',
    function ($routeProvider) { //'snapRemoteProvider',

        //snapRemoteProvider.globalOptions.disable = 'right';
        //snapRemoteProvider.globalOptions.touchToDrag = false;

        $routeProvider.
            when('/',
            {
                templateUrl: 'itemlist.html',
                controller: 'ItemlistCtrl'
            }
        ).
            when('/serialized',
            {
                templateUrl: 'itemlist_serialized.html',
                controller: 'ItemlistSerializeCtrl'
            }
        ).
            otherwise(
            {
                redirectTo: '/'
            }
        );
    }

]);



function parseTemplateUrl(transitions) {
    var count = transitions.length;
    for (var i = 0; i < count; i++) {
        if (transitions[i].item == 'template')
            return transitions[i].to;
    }
}

function applyTransitions2template(template, transitions) {
    var groups = template.groups;
    var countGroups = groups.length;
    for (var iGroup = 0; iGroup < countGroups; iGroup++) {
        var items = groups[iGroup].items;
        var countItems = items.length;
        for (var iItem = 0; iItem < countItems; iItem++) {
            applyTransitions2item(items[iItem], transitions);
        }
    }
}
function applyTransitions2item(item, transitions) {
    var count = transitions.length;
    for (var i = 0; i < count; i++) {
        if (item.label == transitions[i].item) {
            item.state = transitions[i].to;

        }
    }
}


//    return "data/templates/template.json";

function processLinks(string) {
    var firstString = "";
    var secondString = "";
    var retString = "";
    var state = "no";
    for (var i = 0; i < string.length; i++) {
        if (state == "no") {
            if (string[i] == "[") {
                state = "link"
            }
            else {
                retString += string[i];
            }
        }
        else if (state == "link") {
            if (string[i] == "[") {
                if (firstString == "")
                    state = "first";
                else
                    state = "second";
            }
            else if (string[i] == "]") {
                state = "no"
                if (secondString == "") {
                    retString += "<a href=\"" + firstString + "\">" + firstString + "</a>";
                }
                else {
                    retString += "<a href=\"" + firstString + "\">" + secondString + "</a>";
                }
            }
        }
        else if (state == "first") {
            if (string[i] == "]") {
                state = "link";
            }
            else {
                firstString += string[i];
            }
        }
        else if (state == "second") {
            if (string[i] == "]") {
                state = "link";
            }
            else {
                secondString += string[i];
            }
        }

    }
    return retString;


}

function make_id(sentence){
    var ret = "";
    for(var i = 0 ; i< sentence.length ; i++){
        var char1 = sentence.charAt(i);
        var cc = char1.charCodeAt(0);

        if((cc>47 && cc<58) || (cc>64 && cc<91) || (cc>96 && cc<123))
        {
            ret+=char1;
        }
    }
    return ret;
}




