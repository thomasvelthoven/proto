'use strict';


var availableSlots = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17];

function OriginInfo(){

    this.session = uuid.v1();
    this.user = "thomas";
    this.type = "RfItemApp";
    this.version = "0.1";
    var slotRetirementAgeMs = (1000 * 20 * 60); //  (should be 1 hour)
    this.slotSessions = {};
    this.slotTimestamps = {};

    this.slotId = function(time, origin){

        var session = origin.session;
        var calculatedStaleTimeMs = new Date().getTime() - slotRetirementAgeMs;
        if(time == undefined){
            console.log("no without a time");
            // items with no time are considered stale
            return undefined;
        }


        if(time.getTime() < calculatedStaleTimeMs) {
            // items is stale
            return undefined;
        }

        var foundSlotId = this.slotSessions[session];
        if(foundSlotId != undefined){
            // maintain access time to prevent this slot from being cleared
            this.slotTimestamps[foundSlotId] = new Date();
            // return the found slotId
            return foundSlotId;
        }
        else{
            // find a vacant slotId.
            var vacantSlotId = undefined;
            for(var iAvailableSlot=0; iAvailableSlot < availableSlots.length ; iAvailableSlot++){
                var thisSlot = availableSlots[iAvailableSlot];
                var slotTime = this.slotTimestamps[thisSlot];
                if(slotTime == undefined){
                    vacantSlotId = thisSlot;
                    // save some loops
                    break;
                }
            }

            if(vacantSlotId != undefined){
                // claim it
                this.slotSessions[session] = vacantSlotId;
                //console.log("claim slot for session: "+ session + " slot: "+ vacantSlotId);
                // set access time to prevent this slot from being cleared
                this.slotTimestamps[vacantSlotId] = new Date();
                // return the no longer vacant slotId
                return vacantSlotId;
            }
            else{
                // ran out of options
                console.log("no vacant slots ????");
            }
        }
    }
    this.doSlotMaintenance = function(subjectManager){
        var calculatedStaleTimeMs = new Date().getTime() - slotRetirementAgeMs;
        for(var slotId in this.slotTimestamps){
            if(this.slotTimestamps[slotId].getTime() < calculatedStaleTimeMs){
                // this slot should be retired
                delete this.slotTimestamps[slotId];

                // find the sessionid that allocated the slot
                var theSessionToRetire = undefined;
                for(var session in this.slotSessions){
                    var sessionSlotId = this.slotSessions[session];
                    if(sessionSlotId == slotId){
                        theSessionToRetire = session;
                    }
                }
                // do the cleanup
                if(theSessionToRetire != undefined)
                    delete this.slotSessions[theSessionToRetire];

                this.cleanupSessionTraces(theSessionToRetire, slotId, subjectManager);
            }
        }
    }
    this.cleanupSessionTraces = function(retiredSession, retiredSlotId, subjectManager){
        for(var subjectId in subjectManager.subjects){
            selectItem(subjectManager.subjects[subjectId].data, "", retiredSlotId, false);
        }
    }
    this.sendAllHeartBeats = function(subjectManager, $scope){
        for(var subjectId in subjectManager.subjects){
            this.sendHeartBeats(subjectId, subjectManager.subjects[subjectId].data, $scope)
        }
    }
    this.sendHeartBeats = function(subjectId, itemGroup, $scope) {

        for (var iItem = 0; iItem < itemGroup.items.length; iItem++) {

            if (itemGroup.items[iItem].slotId == 0 && itemGroup.items[iItem].selected) {
                var rfEvent = createRfSelectItemEvent(this, itemGroup.items[iItem].id, true);
                $scope.launchRfEvent("rfItemlist", subjectId, rfEvent, true);
            }
        }
        if(itemGroup.groups != undefined){
            for (var iGroup = 0; iGroup < itemGroup.groups.length; iGroup++) {
                this.sendHeartBeats(subjectId, itemGroup.groups[iGroup], $scope);
            }
        }

    }
}






/**
 * throws an error if the parameters cannot result in an event
 * @param originSession identifies the origin session of the event
 * @param originType identifies the type of entity that the event came from
 * @param originVersion identifies the version of entity that the event came from
 * @param name A name of the event that is significant for the originType
 * @param params optional parameters that are valuable to the consumer of the event and are specific to the originType.name
 */
function ensureCorrectRfEventContents(origin, name, params) {
    if(origin == undefined)
        throw Error("origin should be defined with an OriginInfo instance");
    if(origin.user == undefined || origin.user.length == 0)
        throw Error("origin.user should be defined with a string that is not length(0)");
    if(origin.session == undefined || origin.session.length == 0)
        throw Error("origin.session should be defined with a string that is not length(0)");
    if(origin.type == undefined || origin.type.length == 0)
        throw Error("origin.type should be defined with a string that is not length(0)");
    if(origin.version == undefined || origin.version.length == 0)
        throw Error("origin.version should be defined with a string that is not length(0)");
    if(name == undefined || name.length == 0)
        throw Error("name should be defined with a string that is not length(0)");
    if(params == undefined)
        throw Error("expected an array of params");
    return undefined;
}
function ensureCorrectRfEvent(event) {
    return ensureCorrectRfEventContents(event.origin, event.name, event.params)
}
/*
 * convenience method for checking multiple events with ensureCorrectEvent
 * @param events array of objects with the parameters of ensureCorrectEvent in them as properties
 */
function ensureCorrectRfEventsContents(events) {
    if(events == undefined || !Array.isArray(events))
        throw Error("expected an array of events");

    for(var iEvent=0;iEvent<events.length;iEvent++){
        ensureCorrectRfEvent(events[iEvent]);
    }
    return undefined;
}
function removeAllBeforeAndIncludingEvent(events, eventId) {

}
function findAllAfterEvent(events, eventId, deleteFound) {

    // assume that the events are from new to old

    if(events == undefined)
        throw Error("events array expected");

    var ret = [];
    var found = false;
    if(eventId == undefined)
        found = true;
    if(events.length > 0){
        // iterate from old to new
        for(var iEvent = events.length-1 ; iEvent >= 0 ; iEvent--){

            if(found){
                // each later event is more recent and should be in the beginning of the array
                ret.unshift(events[iEvent]);
            }
            else if(events[iEvent].id == eventId){
                // all after this are more recent
                found = true;
            }
        }
    }

    if(found) {
        /*if(deleteFound!=undefined && deleteFound==true){
            // remove the events that have been found from the original array
            for (var iEventRet = 0; iEventRet < ret.length; iEventRet++) {
                for (var iEventArg = 0; iEventArg < events.length; iEventArg++) {
                    if(ret[iEventRet].id == events[iEventArg].id){
                        events.splice(iEventArg, 1);
                        break;
                    }
                }
            }
        }*/
        return ret;
    }
    else
        return undefined;
}
function createRfSetItemEvent(origin, itemId, type, path, label, help) {

    if(origin == undefined)
        throw Error("origin should be defined with an OriginInfo instance");
    if(origin.user == undefined || origin.user.length == 0)
        throw Error("origin.user should be defined with a string that is not length(0)");
    if(origin.session == undefined || origin.session.length == 0)
        throw Error("origin.session should be defined with a string that is not length(0)");
    if(origin.type == undefined || origin.type.length == 0)
        throw Error("origin.type should be defined with a string that is not length(0)");
    if(origin.version == undefined || origin.version.length == 0)
        throw Error("origin.version should be defined with a string that is not length(0)");
    if(type == undefined || type.length == 0)
        throw Error("params.type should be defined with a string that is not length(0)");
    if(itemId == undefined || itemId.length == 0)
        throw Error("params.itemId should be defined with a string that is not length(0)");
    if(label == undefined || label.length == 0)
        throw Error("params.itemId should be defined with a string that is not length(0)");

    var params = {
        itemId: itemId,
        type: type,
        path: path,
        label: label,
        help: help
    }

    return createRfEvent(origin, "setItem", params);
}
function createRfMoveItemEvent(origin, itemId, direction) {

    if(direction != "down" && direction != "up")
        throw Error("direction should be up or down");
    if(origin == undefined)
        throw Error("origin should be defined with an OriginInfo instance");
    if(origin.user == undefined || origin.user.length == 0)
        throw Error("origin.user should be defined with a string that is not length(0)");
    if(origin.session == undefined || origin.session.length == 0)
        throw Error("origin.session should be defined with a string that is not length(0)");
    if(origin.type == undefined || origin.type.length == 0)
        throw Error("origin.type should be defined with a string that is not length(0)");
    if(origin.version == undefined || origin.version.length == 0)
        throw Error("origin.version should be defined with a string that is not length(0)");
    if(itemId == undefined || itemId.length == 0)
        throw Error("id should be defined with a string that is not length(0)");

    var params = {
        itemId: itemId,
        direction: direction
    }

    return createRfEvent(origin, "moveItem", params);
}
function createRfDeleteItemEvent(origin, itemId) {

    if(origin == undefined)
        throw Error("origin should be defined with an OriginInfo instance");
    if(origin.user == undefined || origin.user.length == 0)
        throw Error("origin.user should be defined with a string that is not length(0)");
    if(origin.session == undefined || origin.session.length == 0)
        throw Error("origin.session should be defined with a string that is not length(0)");
    if(origin.type == undefined || origin.type.length == 0)
        throw Error("origin.type should be defined with a string that is not length(0)");
    if(origin.version == undefined || origin.version.length == 0)
        throw Error("origin.version should be defined with a string that is not length(0)");
    if(itemId == undefined || itemId.length == 0)
        throw Error("id should be defined with a string that is not length(0)");

    var params = {
        itemId: itemId
    }

    return createRfEvent(origin, "removeItem", params);
}
function createRfSelectItemEvent(origin, itemId, selected) {

    if(origin == undefined)
        throw Error("origin should be defined with an OriginInfo instance");
    if(origin.user == undefined || origin.user.length == 0)
        throw Error("origin.user should be defined with a string that is not length(0)");
    if(origin.session == undefined || origin.session.length == 0)
        throw Error("origin.session should be defined with a string that is not length(0)");
    if(origin.type == undefined || origin.type.length == 0)
        throw Error("origin.type should be defined with a string that is not length(0)");
    if(origin.version == undefined || origin.version.length == 0)
        throw Error("origin.version should be defined with a string that is not length(0)");
    if(itemId == undefined || itemId.length == 0)
        throw Error("id should be defined with a string that is not length(0)");
    if(selected == undefined)
        throw Error("selected should be defined as true or false");

    var params = {
        itemId: itemId,
        selected: selected
    }

    return createRfEvent(origin, "selectItem", params);
}
function createRfSetItemValueEvent(origin, itemId, state) {

    if(origin == undefined)
        throw Error("origin should be defined with an OriginInfo instance");
    if(origin.user == undefined || origin.user.length == 0)
        throw Error("origin.user should be defined with a string that is not length(0)");
    if(origin.session == undefined || origin.session.length == 0)
        throw Error("origin.session should be defined with a string that is not length(0)");
    if(origin.type == undefined || origin.type.length == 0)
        throw Error("origin.type should be defined with a string that is not length(0)");
    if(origin.version == undefined || origin.version.length == 0)
        throw Error("origin.version should be defined with a string that is not length(0)");
    if(itemId == undefined || itemId.length == 0)
        throw Error("params.itemId should be defined with a string that is not length(0)");
    if(state == undefined || state.length == 0)
        throw Error("params.itemId should be defined with a string that is not length(0)");

    var params = {
        itemId: itemId,
        state: state

    }

    return createRfEvent(origin, "setItemValue", params);
}
function createRfEvent(origin, name, params) {
    ensureCorrectRfEventContents(origin, name, params);

    var newRfEvent = {
        id: uuid.v1(),
        time: makeTimestamp(new Date()),
        name: name,
        origin: origin,
        params: params
    };

    return newRfEvent;
}



/*
function broadcastLocalEventLocally(originSession, originType, name, params) {
}
function broadcastRemoteEventsLocally(url, mostRecentRemoteEventIdFoundLocally) {
    ilEvent.origSessionId = function sessionId;
}
function postLocalEventsRemotely(mostRecentLocelEventIdFoundRemotely) {

}
function recordLocalEventLocally() {

}
function broadcastItemlistEvent(ilEvent) {

    if(ilEvent == undefined)
        throw Error("broadcastItemlistEvent expects an argument: ilEvent");

    function $broadcast(ilEvent.name, [ilEvent]);

}
*/


/*
 Internet Timestamp Generator
 Copyright (c) 2009 Sebastiaan Deckers
 License: GNU General Public License version 3 or later
 */
function makeTimestamp(date) {
    var pad = function (amount, width) {
        var padding = "";
        while (padding.length < width - 1 && amount < Math.pow(10, width - padding.length - 1))
            padding += "0";
        return padding + amount.toString();
    }
    date = date ? date : new Date();
    var offset = date.getTimezoneOffset();
    return pad(date.getFullYear(), 4)
        + "-" + pad(date.getMonth() + 1, 2)
        + "-" + pad(date.getDate(), 2)
        + "T" + pad(date.getHours(), 2)
        + ":" + pad(date.getMinutes(), 2)
        + ":" + pad(date.getSeconds(), 2)
        + "." + pad(date.getMilliseconds(), 3)
        + (offset > 0 ? "-" : "+")
        + pad(Math.floor(Math.abs(offset) / 60), 2)
        + ":" + pad(Math.abs(offset) % 60, 2);
}

function createEventId(){
    return uuid.v1();
}