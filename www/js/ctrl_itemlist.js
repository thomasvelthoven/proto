'use strict';

protoControllers.controller('ItemlistCtrl', ['$scope', '$sce', '$http', '$routeParams', 'originInfo',//,'snapRemote',
    function ($scope, $sce, $http, $routeParams, originInfo) { //, snapRemote

        $scope.checklist_id = $routeParams.checklist_id;
        $scope.subjectId = "2";

        $scope.uiItemSwipedLeft = function (item, edited) {
            if (item.edited == null || !item.edited || edited ) {
                if(item.state != "checked")
                    $scope.editItem($scope.checklist, item, true)
            }
            else{
                $scope.editItem($scope.checklist, item, edited)
            }
            return true;
        };
        $scope.uiItemMoveDown = function (item) {
            var rfEvent = createRfMoveItemEvent(originInfo, item.id, "down");
            $scope.launchRfEvent("rfItemlist", $scope.subjectId, rfEvent, true)
            return true;
        };

        $scope.uiItemMoveUp = function (item) {
            var rfEvent = createRfMoveItemEvent(originInfo, item.id, "up");
            $scope.launchRfEvent("rfItemlist", $scope.subjectId, rfEvent, true)
            return true;
        };

        $scope.uiCheckboxClicked = function (item) {

            var newState;
            if (item.state == 'unchecked' || item.state == null)
                newState = 'checked';
            else
                newState = 'unchecked';

            var rfEvent = createRfSetItemValueEvent(originInfo, item.id, newState);
            $scope.launchRfEvent("rfItemlist", $scope.subjectId, rfEvent, true)

        };


        $scope.uiItemClicked = function (item) {

            var rfEvent;

            if(item.slotId != undefined && item.slotId != 0){
                rfEvent = createRfSelectItemEvent(originInfo, item.id, true);
            }
            else if(item.selected == undefined || item.selected == false){
                rfEvent = createRfSelectItemEvent(originInfo, item.id, true);
            }
            else {
                rfEvent = createRfSelectItemEvent(originInfo, item.id, false);
            }
            $scope.launchRfEvent("rfItemlist", $scope.subjectId, rfEvent, true)

            return true;

        };

        $scope.uiRequestItemRemove = function (item) {

            //$scope.removeItem($scope.checklist, item);
            //$scope.$emit('itemRequestRemove', [item]);

            var rfEvent = createRfDeleteItemEvent(originInfo, item.id);
            $scope.launchRfEvent("rfItemlist", $scope.subjectId, rfEvent, true)

            return true;

        }

        $scope.uiRequestItemAdd = function (index, grouptitle) {
            $scope.$emit('itemRequestAdding', [index, grouptitle]);
        };

        $scope.uiRequestItemEditing = function (item) {
            $scope.$emit('itemRequestEditing', [item]);
        };

        $scope.editItem = function (itemgroup, item, edited) {
            for(var i=0;i<itemgroup.items.length;i++)
            {
                if(edited){
                    if(item.label == itemgroup.items[i].label) {
                        itemgroup.items[i].edited = true;
                    }
                    else {
                        itemgroup.items[i].edited = undefined;
                    }
                }
                else
                    itemgroup.items[i].edited = undefined;

            }
            if(itemgroup.groups != undefined){
                for(i=0;i<itemgroup.groups.length;i++){
                    $scope.editItem(itemgroup.groups[i], item, edited)
                }
            }
        }


        $scope.ifChecked = function (item, checkedString, uncheckedString) {
            if(item.state='checked')
                return checkedString
            else
                return uncheckedString;
        }



        $scope.toggleHelp = function (item, state) {

            if(item && item.help){
                var selector = '#item_div_'+ item.id;
                $(selector).popover({
                    "placement" : 'auto',
                    "selector" : '[rel="popover"]',
                    "trigger" : 'manual',
                    "title": item.label,
                    "content" : item.help
                });
                //$scope.selectedItem = item
                //$scope.$emit('itemSelected', item)


                $(selector).popover(state);
            }
        };


        $('#myModal').on('hidden.bs.modal', function (e) {
            var event = createEventForItem(
                $scope.item2bedited.group,
                $scope.item2bedited.id,
                $scope.item2bedited.label,
                $scope.item2bedited.type,
                $scope.item2bedited.help
            );
            $scope.$broadcast("eventSetItem", [event]);

            var rfEvent = createRfSetItemEvent(
                originInfo,
                $scope.item2bedited.id,
                $scope.item2bedited.type,
                $scope.item2bedited.group,
                $scope.item2bedited.label,
                $scope.item2bedited.help);

            $scope.launchRfEvent("rfItemlist", $scope.subjectId, rfEvent, true)

        })

        $scope.$on("eventSetItemValue", function(event, data){

            if(data.length != 1)
                throw Error("eventSetItemValue should have one event item as an argument");

            var eventSetItemValue = data[0];

            var item = findItemWithId($scope.checklist, eventSetItemValue.params.id);

            if (item.state != 'checked' && eventSetItemValue.params.state == 'checked') {
                item.state = 'checked';
                $scope.selectItem($scope.checklist, item, true, true, false)
            }
            else if (item.state != 'unchecked' && eventSetItemValue.params.state == 'unchecked') {
                item.state = 'unchecked';
                $scope.selectItem($scope.checklist, item, true, false, false)
            }

        });
        $scope.$on('eventSetItem', function(event, data) {

            if(data.length != 1)
                throw "expected only one argument from event new_item got:"+data.length;

            var eventSetItem = data[0];

            $scope.$apply(function () {
                ensureItem(
                    $scope.checklist,
                    eventSetItem.params.id,
                    eventSetItem.params.label,
                    eventSetItem.params.help,
                    eventSetItem.params.type,
                    eventSetItem.params.path);
            });

        });

    }


]);


protoControllers.controller('ItemlistSerializeCtrl', ['$scope', '$sce', '$http', '$routeParams',//,'snapRemote',
    function ($scope, $sce, $http, $routeParams) { //, snapRemote

        $scope.checklist_id = $routeParams.checklist_id;

        //createEventsForGroup($scope.checklist, "", $scope.checklistrecords);

        $scope.getRecords = function (group) {
            if(group != undefined)
            {
                $scope.checklistrecords = [];
                createEventsForGroup(group, "", $scope.checklistrecords);
            }
            return $scope.checklistrecords;
        };


    }

]);
