
var machine = new StateMachine("machine");

var controller = new Region("itemconn", machine);

var initial = new PseudoState("initial", PseudoStateKind.Initial, itemconn);
var get_events = new CompositeState("get_events", itemconn);
var post_events = new CompositeState("post_events", itemconn);
var idle = new SimpleState("idle", itemconn);

var traversing_archives = new CompositeState("traversing_archives", get_events);

var to_previous = new SimpleState("to_previous", traversing_archives);
var to_current = new CompositeState("to_current", traversing_archives);

var revisit_known_events = new SimpleState("revisit_known_events", to_current);
var consume_new_events = new SimpleState("consume_new_events", to_current);

var t0 = new Transition(initial, get_events);
var t0 = new Transition(initial, post_events);
var t2 = new Transition(get_events, post_events, function (s, c) { return c === "done_getting_events"; });
var t2 = new Transition(post_events, idle, function (s, c) { return c === "done_posting_events"; });

var t2 = new Transition(get_events, idle, function (s, c) { return c === "error"; });
var t2 = new Transition(post_events, idle, function (s, c) { return c === "error"; });

var t2 = new Transition(idle, get_events, function (s, c) { return c === "wakeup"; });

var t2 = new Transition(get_events, to_previous, function (s, c) { return c === "last_event_not_found"; });
