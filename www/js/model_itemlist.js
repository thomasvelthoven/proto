'use strict';

function createItemId(){
    return uuid.v1();
}
String.prototype.ensureIsGroupPath = function() {

    if(this=='')
        return;

    /*var regExp = /^[\. A-Za-z0-9\-]+$/;
    if(!this.match(regExp))
        throw "a group path must only contain alphanumerics, spaces or periods got "+this;*/
};
function ensureItem(rootContainer, id ,label, help, type, groupPathString, positionId, before) {

    if(undefined == rootContainer)
        throw Error("rootContainer must have a value")
    if(undefined == id)
        throw Error("id must have a value")
    if(undefined == label)
        throw Error("label must have a value")
    if(undefined == type)
        throw Error("type must have a value")


    var founditem;
    var group = ensureGroup(rootContainer, groupPathString);
    founditem = findItemWithId(rootContainer, id)
    var item;
    if(founditem == undefined)
        item = createNewItem(id, label, help, type);
    else
        item = founditem
    item.label = label;
    item.help = help;
    item.type = type;

    if(founditem == undefined)
        group.items.push(item);
}
function ensureGroup(container, groupPathString){
    var groupPathArray = parseGroups(groupPathString);
    return ensureGroupFromGroupArray(container, groupPathArray, 0)
}
function ensureGroupFromGroupArray(container, groupPathArray, indent){


    if(container  == undefined)
        throw "ensureGroup groups must hold array";
    if(groupPathArray == undefined)
        throw "ensureGroup groupPathArray must hold array";

    if(groupPathArray.length == 0)
        return container;

    var group2find = groupPathArray[indent];
    // first see if there is a group here where label == groupPathArray[indent]
    var groupFound;
    for(var i=0;i<container.groups.length;i++) {
        if (container.groups[i].title == group2find) {
            groupFound = container.groups[i];
        }
    }
    if(groupFound == undefined){
        var outerGroup;
        var groupContainer = container.groups;
        var outerGroup = undefined;
        for(var i=indent;i<groupPathArray.length;i++){
            outerGroup = createNewGroup(groupPathArray[i])
            groupContainer.push(outerGroup);
            groupContainer = outerGroup.groups;
        }
        return outerGroup;
    }
    else{
        if((indent + 1) == groupPathArray.length)
            return groupFound;
        else
            return ensureGroupFromGroupArray(groupFound.groups, groupPathArray, indent+1)
    }


}

function createNewItem(id, label, help, type){

    return { id: id, label: label, help: help, type: type, toString: function() {
        return "Item{id: "+this.id+" label: " + this.label + " , type: " + this.type +"}";
    }};
}
function createNewGroup(title, subtitle){
    var group = { title: title, subtitle: subtitle, groups: [], items: [], toString: function() {
        return "Group{ title: " + this.title + " , " + this.groups.length + " subgroups: , "+ this.items.length + " items }";
    }};
    return group;
}
function parseGroups(groupPath){
    var groups = [];
    var char = "";

    if(groupPath==undefined)
        return groups;

    groupPath.ensureIsGroupPath();

    var current = "";
    for(var i=0;i<groupPath.length;i++){
        if(groupPath[i] == "."){
            var trimmedCurrent = current.trim();
            current = "";
            if(trimmedCurrent.length > 0) {
                groups.push(trimmedCurrent);
            }
        }
        else
            current += groupPath[i];
    }
    var trimmedCurrent = current.trim();
    if(trimmedCurrent.length > 0) {
        groups.push(trimmedCurrent);
    }
    return groups;
}
function createEventsForGroup(group, path, target){

    if(group == undefined)
        throw Error("group must be an object");

    if(group == undefined)
        throw Error("target must be a string");

    if(path == undefined)
        path = "";

    var pathBase = "";
    if(path != "")
        pathBase = path + ".";

    target.push(createEventSetTitle(group.title, path));

    target.push(createEventSetSubtitle(group.subtitle, path));

    for(var iItem = 0 ; iItem < group.items.length ; iItem++){
        var events = createEventsForItem(group.items[iItem], path);
        for(var iEvent=0;iEvent < events.length ; iEvent++)
            target.push(events[iEvent]);
    }


    if(group.groups != undefined) {
        for (var iGroup = 0; iGroup < group.groups.length; iGroup++) {

           createEventsForGroup(group.groups[iGroup], pathBase + group.groups[iGroup].title , target);
        }
    }
}

function createEventSetTitle(aTitle, path){

    var theTitle = "";
    if(aTitle!=undefined)
        theTitle = aTitle;

    return {
        name: "setItem",
        params: {
            type: "title",
            path: path,
            label: aTitle
        }
    };
}
function createEventSetSubtitle(aSubTitle, path){

    var theSubTitle = "";
    if(aSubTitle!=undefined)
        theSubTitle = aSubTitle;

    return {
        name: "setItem",
        params: {
            type: "subtitle",
            path: path,
            label: aSubTitle
        }
    };
}
function createEventsForItem(item, path){

    var setItemEvent = createEventForItem(path, item.label, item.type, item.help)

    if(item.type=='check' && item.state=="checked")
        return [setItemEvent, createEventForItemState(item, path, item.state)];
    else
        return [setItemEvent];

}


function createEventForItem(path, id, label, type, help){

    return {
        name: "setItem",
        params: {
            type: type,
            id: id,
            label: label,
            help: help,
            path: path
        }
    }
}

function createEventForItemState(item, path, state){

    return {
        name: "setItemValue",
        params: {
            label: item.label,
            id: item.id,
            state: state
        }
    };
}

function findItemWithId(group, id){

    if(id == undefined || id.length == 0)
        throw Error("id must hold a string");

    for(var iItem=0;iItem<group.items.length;iItem++) {
        if(group.items[iItem].id == id)
            return group.items[iItem];
    }

    if(group.groups != undefined){
        for(var iGroup=0;iGroup<group.groups.length;iGroup++) {
            var itemFound =
                findItemWithId(group.groups[iGroup],id);
            if(itemFound != undefined)
                return itemFound;

        }
    }
    return undefined;

}
function findItemWithLabel(group, label){

    if(group  == undefined)
        throw "ensureGroup groups must hold array";
    if(label == undefined || label.length == 0)
        throw "label must hold a string";

    for(var iItem=0;iItem<group.items.length;iItem++) {
        if(group.items[iItem].label == label)
            return group.items[iItem];
    }

    if(group.groups != undefined){
        for(var iGroup=0;iGroup<group.groups.length;iGroup++) {
            var itemFound =
            findItemWithLabel(group.groups[iGroup], label);
            if(itemFound != undefined)
                return itemFound;

        }
    }
    return undefined;

}

function applyItemlistEvent(itemlist, rfEvent, slotId, user){


    if(rfEvent.name == "setItem")
        applyItemlistSetItem(itemlist, rfEvent.params)
    else if(rfEvent.name == "setItemValue")
        applyItemlistSetItemValue(itemlist, rfEvent.params)
    else if(rfEvent.name == "removeItem")
        applyItemlistDeleteItem(itemlist, rfEvent.params)
    else if(rfEvent.name == "moveItem")
        applyItemlistMoveItem(itemlist, rfEvent.params)
    else if(rfEvent.name == "selectItem")
        applyItemlistSelectItem(itemlist, rfEvent.params, slotId, user)
    else if(rfEvent.name != "setItemlist")
        throw Error("unknown eventname: " + rfEvent.name)


}
function applyItemlistSetItem(itemlist, params){

    console.log("setItem("+params.itemId+")");
    var item =
        ensureItem(
            itemlist,
            params.itemId ,
            params.label,
            params.help,
            params.type,
            params.groupPathString,
            params.positionId,
            params.before);
}
function applyItemlistSetItemValue(itemlist, params){
    var item = findItemWithId(itemlist, params.itemId)
    if(item == undefined)
        throw Error("item not found: setItemValue("+params.itemId+"="+params.state+")");

    if(params.state == undefined)
        throw Error("params.state value expected");

    item.state = params.state;

}
function applyItemlistDeleteItem(itemlist, params){
    var item = findItemWithId(itemlist, params.itemId)
    if(item == undefined)
        throw Error("DeleteItem requires item to exist id: "+params.itemId);

    removeItem(itemlist, item);

}

function applyItemlistMoveItem(itemlist, params){

    var item = findItemWithId(itemlist, params.itemId)
    if(item == undefined)
        throw Error("MoveItem requires item to exist id: "+params.itemId);

    if(params.direction == "down")
        moveItemDown(itemlist, item);
    else if(params.direction == "up")
        moveItemUp(itemlist, item);
    else
        throw Error("params.direction should be up or down");
}


function applyItemlistSelectItem(itemlist, params, slotId, user){
    var item = findItemWithId(itemlist, params.itemId)

    if(item == undefined)
        throw Error("SelectItem requires item to exist id: "+params.itemId);

    if (item.state == "checked") {
        selectItem(itemlist, item.id, slotId, user, false)
    }
    else{
        selectItem(itemlist, item.id, slotId, user, params.selected)
    }


}
function selectItem(itemgroup, itemId, desiredSlotId, desiredUser, selected) {

    if(desiredSlotId ==undefined)
        console.log("desiredSlotId undefined");

    for(var i=0;i<itemgroup.items.length;i++)
    {
        //var item_div_id = "#item_div_"+itemgroup.items[i].id;

        if(itemId == itemgroup.items[i].id) {
            if(selected){
                itemgroup.items[i].slotId = desiredSlotId;
                itemgroup.items[i].user = desiredUser;
                itemgroup.items[i].selected = true;

            }
            else{
                itemgroup.items[i].selected = false;
                itemgroup.items[i].slotId = undefined;
                itemgroup.items[i].user = undefined;
            }
        }
        // unselect all else except if from different slotId
        else if(itemgroup.items[i].selected){
            // should we unselect ?
            if(itemgroup.items[i].slotId == undefined || itemgroup.items[i].slotId == desiredSlotId){
                // yes because same slotId or no slotid
                itemgroup.items[i].selected = false;
                itemgroup.items[i].slotId = undefined;
                itemgroup.items[i].user = undefined;
            }
        }


    }
    if(itemgroup.groups != undefined){
        for(var i=0;i<itemgroup.groups.length;i++){
            selectItem(itemgroup.groups[i], itemId, desiredSlotId, desiredUser, selected)
        }
    }
}

function removeItem(itemgroup, item) {

    var indexFound = -1;
    for(var i=0;i<itemgroup.items.length;i++)
    {
        if(item.id == itemgroup.items[i].id) {
            indexFound = i;
            item.removed = true;
            break;
        }
    }
    if(indexFound >= 0){
        return indexFound;
    }
    else {
        if (itemgroup.groups != undefined) {
            for (i = 0; i < itemgroup.groups.length; i++) {
                var indexFound = removeItem(itemgroup.groups[i], item)
                if (indexFound >= 0)
                    return indexFound;
            }
        }
    }
}
function moveItemUp(itemgroup, itemToMove) {
    var positions = cutItem(itemgroup, itemToMove.id, 0);
    if(positions.cutPosition == -1)
        throw Error("item not found"+itemIdToMove.id);

    var insertPosition;
    if(positions.cutPosition == 1)
        insertItem(itemgroup, itemToMove, 1, 0)
    else
        insertPosition = positions.cutPosition-1
    insertItem(itemgroup, itemToMove, insertPosition, 0)

}
function moveItemDown(itemgroup, itemToMove) {
    var positions = cutItem(itemgroup, itemToMove.id, 0);
    if(positions.cutPosition == -1)
        throw Error("item not found"+itemIdToMove.id);


    var insertPosition;
    if(positions.cutPosition == positions.endPosition){
        insertPosition = positions.endPosition;
    }
    else{
        insertPosition = positions.cutPosition+1
    }
    insertItem(itemgroup, itemToMove, insertPosition, 0)

}
function cutItem(itemgroup, itemIdToMove, startPosition) {

    var cutPosition = -1;
    var localPosition = startPosition + 1;

    var itemPosition = 0;
    var splicePosition = -1;
    // +1 for the group header
    for(var i=0;i<itemgroup.items.length;i++)
    {
        itemPosition = i;
        // +1 for each item
        if(itemIdToMove == itemgroup.items[i].id) {
            // at position i remove 1 item
            // postpone the splice til after the iteration
            splicePosition = itemPosition;
        }
    }
    if(splicePosition != -1){
        // remember the position the slot that we left empty
        itemgroup.items.splice(splicePosition, 1);
        cutPosition = localPosition + splicePosition;
    }

    var groupPosition = localPosition  + itemPosition;
    if (itemgroup.groups != undefined) {
        for (var i = 0; i < itemgroup.groups.length; i++) {
            var positions = cutItem(itemgroup.groups[i], itemIdToMove, groupPosition)
            groupPosition = positions.endPosition;
            if(positions.cutPosition != -1)
                cutPosition = positions.cutPosition;
        }
    }

    return {
        endPosition: groupPosition,
        cutPosition: cutPosition
    };


}
function insertItem(itemgroup, itemToInsert, insertPosition, currentPosition) {

    // +1 for the group header
    var itemPosition = currentPosition + 1;

    var itemOffset = insertPosition - itemPosition;
    // insert here?
    if(itemOffset <= itemgroup.items.length){

        itemgroup.items.splice(itemOffset, 0, itemToInsert);
        // this group contained the position
        // the item has been inserted
        // returning that message to the caller
        return -1;
    }

    // the item is not in this groups local items.
    // maybe in one of its child groups
    // increase the local position with the amount of local items
    var localPosition = itemgroup.items.length + itemPosition;

    if(itemgroup.groups != undefined){
        for (var i = 0; i < itemgroup.groups.length; i++) {

            // add the positions per group recursively
            localPosition = insertItem(itemgroup.groups[i], itemToInsert, insertPosition, localPosition)
            if(localPosition == -1)
                // this group or one of its subgroups contained the position
                // the item has been inserted
                // returning that message to the caller
               return -1;
        }
    }
    // the item has not been inserted.
    // Returning the last local position to the caller
    return localPosition;
}